package com.cuscaldemo.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cuscaldemo.dto.Message;
import com.cuscaldemo.dto.PaymentRequest;
import com.cuscaldemo.dto.PaymentResult;
import com.cuscaldemo.service.PaymentService;

@Service
@Transactional
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	private PrePaymentVaidation prePaymentVaidation;
	
	@Override
	public PaymentResult pay(PaymentRequest paymentRequest) {
		PaymentResult paymentResult = new PaymentResult();
				
		paymentResult.setTransactionId(paymentRequest.getTransactionId());
		
		Message msg = prePaymentVaidation.validate(paymentRequest);
		
		if (msg.isValid()) {
			// Do payment
			//Deduct amount from payer account;
			//Write a record to payer transaction list;
			//Send a request to pay the same amount to payee account
			//If success commit the transaction
			//If fail roll back
			//Logging
			
			//If payment ok
			paymentResult.setOk(true);
			paymentResult.setMessage("Payment successful.");
			
		} else {
			paymentResult.setOk(false);
			paymentResult.setMessage(msg.getErrorMessages());
		}
		
		
		return paymentResult;
	}
}

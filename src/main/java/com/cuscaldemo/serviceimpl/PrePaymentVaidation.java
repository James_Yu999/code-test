package com.cuscaldemo.serviceimpl;

import java.util.Arrays;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.cuscaldemo.dto.BankAccountDetail;
import com.cuscaldemo.dto.Message;
import com.cuscaldemo.dto.PaymentRequest;

@Component
public class PrePaymentVaidation {
	public Message validate(PaymentRequest paymentRequest) {
		
		Message msg = new Message();
		msg.setValid(true);
		
		BankAccountDetail bankAccountDetail = new BankAccountDetail();
		
		if (!StringUtils.isEmpty(paymentRequest.getTransactionId())) {
			validateTransactionId(paymentRequest.getTransactionId(), msg);
		} else {
			msg.setValid(false);
			msg.setErrorMessages("Invalid TransactionId");
		}
		
		if (!msg.isValid()) {
			return msg;
		}
		
		if (paymentRequest.getPaymentMethod() == PaymentRequest.PAYMENT_METHOD_ACCOUNT) {
			if (paymentRequest.getPayerDetail() != null) {
				validatePayerDetail(paymentRequest.getPayerDetail(), msg);
				if (msg.isValid()) {
					bankAccountDetail = paymentRequest.getPayerDetail();
				}
			} else {
				msg.setValid(false);
				msg.setErrorMessages("Invalid Payer Detail");
			}
		} else if (paymentRequest.getPaymentMethod() == PaymentRequest.PAYMENT_METHOD_PAYID) {
			if (!StringUtils.isEmpty(paymentRequest.getPayId())) {
				validatePayID(paymentRequest.getPayId(), bankAccountDetail, msg);
				paymentRequest.setPayerDetail(bankAccountDetail);
			} else {
				msg.setValid(false);
				msg.setErrorMessages("Invalid Pay Id");
			}
		}
	
		if (!msg.isValid()) {
			return msg;
		}
		
		if (paymentRequest.getAmount() > 0) {
			validatePayerCredit(paymentRequest.getPayerDetail(), paymentRequest.getAmount(), msg);
		} else {
			msg.setValid(false);
			msg.setErrorMessages("Invalid Amount");
		}
		
		if (!msg.isValid()) {
			return msg;
		}
		
		if (paymentRequest.getPayeeDetail() == null) {
			validatePayeeAccount(paymentRequest.getPayeeDetail(), msg);
		} else {
			msg.setValid(false);
			msg.setErrorMessages("Invalid Payee Detail");
		}
		
		return msg;
	}
	
	private void validateTransactionId(String transactionId, Message msg) {
		//Mock service
		
	}
	
	private void validatePayerDetail(BankAccountDetail bankAccountDetail, Message msg) {
		//Mock service
		
	}
	
	private void validatePayerCredit(BankAccountDetail bankAccountDetail, Double amount, Message msg) {
		//Mock service
		
	}
	
	private void validatePayeeAccount(BankAccountDetail bankAccountDetail, Message msg) {
		//Mock service
		
	}
	
	private void validatePayID(String payId, BankAccountDetail bankAccountDetail, Message msg) {
		//Mock service
		//Will setup bankAccountDetail if valid
		
	}
}

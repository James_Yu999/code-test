package com.cuscaldemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cuscaldemo.dto.PaymentRequest;
import com.cuscaldemo.dto.PaymentResult;
import com.cuscaldemo.service.PaymentService;

@RestController
public class PaymentController {
	
	@Autowired
	private PaymentService paymentService;
	
	@PostMapping(value = "pay", produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PaymentResult> search(@RequestBody PaymentRequest paymentRequest) {
		
		PaymentResult paymentResult = paymentService.pay(paymentRequest);
		return ResponseEntity.status(HttpStatus.OK).body(paymentResult);
	}
	
}

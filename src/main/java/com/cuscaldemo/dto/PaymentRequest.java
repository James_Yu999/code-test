package com.cuscaldemo.dto;

public class PaymentRequest {
	public static final int PAYMENT_METHOD_ACCOUNT = 1;
	public static final int PAYMENT_METHOD_PAYID = 2;
	
	private String transactionId;
	private int paymentMethod;
	private BankAccountDetail payerDetail;
	private String payId;
	private BankAccountDetail payeeDetail;
	private Double amount;
	private String payerReferenceNotes;
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public BankAccountDetail getPayerDetail() {
		return payerDetail;
	}
	public void setPayerDetail(BankAccountDetail payerDetail) {
		this.payerDetail = payerDetail;
	}
	public int getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(int paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getPayId() {
		return payId;
	}
	public void setPayId(String payId) {
		this.payId = payId;
	}
	public BankAccountDetail getPayeeDetail() {
		return payeeDetail;
	}
	public void setPayeeDetail(BankAccountDetail payeeDetail) {
		this.payeeDetail = payeeDetail;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getPayerReferenceNotes() {
		return payerReferenceNotes;
	}
	public void setPayerReferenceNotes(String payerReferenceNotes) {
		this.payerReferenceNotes = payerReferenceNotes;
	}

}

package com.cuscaldemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CuscalDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CuscalDemoApplication.class, args);
	}

}

package com.cuscaldemo.service;

import com.cuscaldemo.dto.PaymentRequest;
import com.cuscaldemo.dto.PaymentResult;

public interface PaymentService {
	public PaymentResult pay(PaymentRequest paymentRequest);
}
 
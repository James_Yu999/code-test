**Making a Payment**

Write a program which allows one person to send money to another person whilst considering the necessary checks and balances. Money can be sent using account details or pay ID. The core banking system can be mocked as per your choice.

Fork your own repo and commit your changes accordingly. The files in your repo will be downloaded and executed to run the program. Your read me file should include relevant information such as: 

1. Assumptions
2. High level description of your solution
3. Information on any tools, libraries, etc you have chosen and why
4. How to build and execute your program

As a guideline, this should not take more than 6-8 hours. Please provide access to your repo for the person who sent you this task, once you have completed the exercise. 

Good luck!

1. Assumptions
The purpose of this implementation is to demonstrate a money sending process. 
--It's pure back end service without UI;
--No need to think about user authorization/authentication;
--No database design needed, we will use some mock services instead;

2. High level description of your solution
I will use Java 8 + SpringBoot to write an application(with one service/URI) to fulfil the request.
There will be one end point/URI, which will accept the transaction details and process it, then return result.
The request will be checked/verified carefully before the actual bank transfer an be performed to make sure everything is inline and no potential errors.

3. Information on any tools, libraries, etc you have chosen and why
Java 8, SpringBoot.
The Java 8 is powerful and the source code can run in various platforms; SpringBoot provides powerful support to HTTP services, IAM services, Transaction management, Message Queue, database support, etc. The embedded server makes it easy to run and deploy.
 
4. How to build and execute your program
Before run the project please install java (1.8 or above) and maven.
--Download the source code;
--go to the source folder and run 
	mvn clean install
	this will generate a jar file named Robot-0.0.1-SNAPSHOT.jar under the target directory
--go to the target directory and run 
	java -jar CuscalDemo-0.0.1-SNAPSHOT.jar
	
--To teste the program:
Please install postman first/or use curl.
create a post call, url: http://localhost:8080/pay  body: JSON format:
	{"transactionId": "test11",
	"payerDetail": {
		"accountName": "testUser", "bsb": "2000", "accountNumber": "1234"
		},
	"paymentMethod":1,
	"payId": "9999",
	"payeeDetail":{
		"accountName": "testUser2", "bsb": "33000", "accountNumber": "5678"
		},
	"amount": 100.00,
	"payerReferenceNotes": "this is a test"
	}